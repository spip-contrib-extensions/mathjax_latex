# MathJax Latex pour SPIP

Ce plugin est déprécié : le plugin MathJax v2 reprend le modèle `<math|f=...>` de ce plugin et permet de couvrir tous les usages

https://git.spip.net/spip-contrib-extensions/mathjax
